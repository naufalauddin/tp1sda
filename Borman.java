import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import java.util.Scanner;
import java.util.HashMap;
import java.util.ArrayList;

public class Borman {
    private static BufferedReader in; 
    private static PrintWriter out;

    private static HashMap<String, HashMap> toko = new HashMap<String, ArrayList>();
    public static void main(String[] args) {
        // taken from ws2Jumat template
        InputStream inputStream = System.in;
        in = new BufferedReader(new InputStreamReader(inputStream), 32768);
        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);

        readInput();
        out.close();
    }

    /**
     * a method to read all the input and simulate day to output a result
     */
    private static void readInput() {
        int jumlahToko = Integer.parseInt(in.readLine());
        readToko(jumlahToko);
        
        int jumlahHari = Integer.parseInt(in.readLine());
        for (int i = 0; i < jumlahHari; i++) {
            simulateDay();
        }
    }

    private static void printOutput(String output) throws IOException{
        out.println(output);
    }

    private static void simulateDay() {
        int jumlahTokoBuka = Integer.parseInt(in.readLine());
        String[] tokoYangBuka = in.readLine().split(" ");

        int target = Integer.parseInt(in.readLine().split(" ")[1]);
        int cara = calculateCara(target);
        printOutput("" + cara);

        int jumlahDuar = Integer.parseInt(in.readLine().split(" ")[1]);
        doDuar(jumlahDuar);

        int jumlahRestock = Integer.parseInt(in.readLine().split(" ")[1]);
        doRestock(jumlahRestock);

        int jumlahTransfer = Integer.parseInt(in.readLine().split(" ")[1]);
        doTransfer(jumlahTransfer);
    }

    /**
     * A function for store @param jumlahToko toko into toko hasmap
     */
    private static void readToko(int jumlahToko) {
        for (int i = 0; i < jumlahToko; i++) {
            String line = in.readLine();
            String[] query = line.split(" ");
            String namaToko = query[0];
            int jumlahDonat = Integer.parseInt(query[1]);
            stockDonat(namaToko, jumlahToko);
        }
    }

    /**
     * a method to stock up donat as hashmap into toko in toko hashmap
     * @param namaToko is the name of the toko as a key in the hashmap
     * @param jumlahDonat is the number of types of donat the toko has
     */
    private static void stockDonat(String namaToko, int jumlahDonat) {
        HashMap<String, Donat> donatYangDijual = new HashMap<>();
        for (int i = 0; i < jumlahDonat; i++) {
            String line = in.readLine();
            String[] query = line.split(" ");
            String namaDonat = query[0];
            int stock = Integer.parseInt(query[1]);
            int jumlahChocochip = Integer.parseInt(query[2]);
            Donat donat = new Donat(namaDonat, stock, jumlahChocochip);
            donatYangDijual.put(namaDonat, donat);
        }
        toko.put(namaToko, donatYangDijual);
    }

    private static int calculateCara(int target) {
        return 0;
    }

    private static void doDuar(int jumlahDuar) {
        for (int i = 0; i < jumlahDuar; i++) {
            String[] query = in.readLine().split(" ");
            String namaToko = query[0];
            String namaDonat = query[1];
            int jumlahYangMeledak = Integer.parseInt(query[2]);
            toko.get(namaToko).get(namaDonat).addStock(-jumlahYangMeledak);
        }
    }

    private static void doRestock(int jumlahRestock) {
        for (int i; i < jumlahRestock; i++) {
            String[] query = in.readLine().split(" ");
            String namaToko = query[0];
            String namaDonat = query[1];
            int jumlahYangDirestock = Integer.parseInt(query[2]);
            int jumlahChocochip = Integer.parseInt(query[3]);
            toko.get(namaToko).get(namaDonat).reStock(jumlahYangDirestock, jumlahChocochip);
        }
    }

    private static void doTransfer(int jumlahTransfer) {
        for (int i; i < jumlahTransfer; i++) {
            String[] query = in.readLine().split(" ");
            String namaAsal = query[0];
            String namaTujuan = query[1];
            String namaDonat = query[2];
            int jumlahYangDirestock = Integer.parseInt(query[3]);
            int jumlahChocochip = toko.get(namaAsal).get(namaDonat).getJumlahChocochip();
            toko.get(namaTujuan).get(namaDonat).reStock(jumlahYangDirestock, jumlahChocochip);
        }
    }

    private class Donat {
        private String nama;
        private int stock;
        private int jumlahChocochip;

        public Donat(String nama, int stock, int jumlahChocochip) {
            this.nama = nama;
            this.stock = stock;
            this.jumlahChocochip = jumlahChocochip;
        }

        public String getNama() {
            return nama;
        }

        public boolean reStock(int jumlah, int jumlahChocochip) {
            if (stock == 0) {
                this.jumlahChocochip = jumlahChocochip;
            } else {
                if (jumlahChocochip != this.jumlahChocochip) {
                    return false;
                }
            }
            this.stock += jumlah;
            return true;
        }

        public void addStock(int jumlah) {
            reStock(jumlah, jumlahChocochip);
        }

        public int getJumlahChocochip() {
            return jumlahChocochip;
        }

        public void setStock(int jumlah) {
            stock = jumlah;
        }

        public int getStock() {
            return stock;
        }
    }

}